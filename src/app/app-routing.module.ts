import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AngestellterComponent } from './angestellter/angestellter/angestellter.component';
import { FreiberuflerComponent } from './freiberufler/freiberufler/freiberufler.component';
import { ManagerComponent } from './manager/manager/manager.component';
import { MitarbeiterComponent } from './mitarbeiter/mitarbeiter/mitarbeiter.component';
import { EmployeesComponent } from './employees/employees/employees.component';
import { DepartmentComponent } from './department/department/department.component';
import { AddFreiberuflerComponent } from './freiberufler/add-freiberufler/add-freiberufler.component';
import { AddangestellterComponent } from './angestellter/add-angestellter/add-angestellter.component';
import { AddDepartmentComponent } from './department/add-department/add-department.component';
import { AddMitarbeiterComponent } from './mitarbeiter/add-mitarbeiter/add-mitarbeiter.component';
import { AddmanagerComponent } from './Manager/add-manager/add-manager.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: MitarbeiterComponent },
  { path: 'add-freiberufler', component: AddFreiberuflerComponent },
  { path: 'add-angestellter', component: AddangestellterComponent },
  { path: 'add-department', component: AddDepartmentComponent },
  { path: 'add-mitarbeiter', component: AddMitarbeiterComponent },
  { path: 'add-manager', component: AddmanagerComponent },

  { path: 'Angestellter', component: AngestellterComponent },
  { path: 'Freiberufler', component: FreiberuflerComponent },
  { path: 'Manager', component: ManagerComponent },
  { path: 'Mitarbeiter', component: MitarbeiterComponent },
  { path: 'employees', component: EmployeesComponent },
  { path: 'Department', component: DepartmentComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
