import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Department } from '../department';
import { DepartmentService } from '../department.service';
@Component({
  selector: 'app-add-department',
  templateUrl: './add-department.component.html',
  styleUrls: ['./add-department.component.css']
})
export class AddDepartmentComponent implements OnInit {

  constructor(private departmentservice:DepartmentService) { }

  department : Department=new Department();
  submitted = false;

  ngOnInit() {
    this.submitted=false;
  }

  departmentsaveform=new FormGroup({
    department_name:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    department_manager:new FormControl('' , [Validators.required , Validators.minLength(5) ] )
  });

  saveDepartment(saveDepartment){
    this.department=new Department();   
    this.department.dep_name = this.departmentName.value;
    this.department.dep_manager = this.departmentManager.value;
    this.submitted = true;
    this.save();
  }

  

  save() {
    this.departmentservice.createDepartment(this.department)
      .subscribe(data => console.log(data), error => console.log(error));
    this.department = new Department();
  }

  get departmentName(){
    return this.departmentsaveform.get('department_name');
  }
  get departmentManager(){
    return this.departmentsaveform.get('department_manager');
  }

  addDepartmentForm(){
    this.submitted=false;
    this.departmentsaveform.reset();
  }
}
