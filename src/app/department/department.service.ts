import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {

  private baseUrl = 'http://localhost:8080/';  
  
  constructor(private http:HttpClient) { } 

  getDepartmentList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'department');  
  }  
  
  createDepartment(department: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'department', department);  
  }  
  
  deleteDepartment(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/department/${id}`, { responseType: 'text' });  
  }  
  
  getDepartment(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/department/${id}`);  
  }  
  
  updateDepartment(id: number, value: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}/department/${id}`, value);  
  }
}
