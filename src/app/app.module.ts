import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import {DataTablesModule} from 'angular-datatables';
import { AngestellterComponent } from './angestellter/angestellter/angestellter.component';
import { FreiberuflerComponent } from './freiberufler/freiberufler/freiberufler.component';
import { ManagerComponent } from './manager/manager/manager.component';
import { MitarbeiterComponent } from './mitarbeiter/mitarbeiter/mitarbeiter.component';
import { EmployeesComponent } from './employees/employees/employees.component';
import { DepartmentComponent } from './department/department/department.component';
import { AddMitarbeiterComponent } from './mitarbeiter/add-mitarbeiter/add-mitarbeiter.component';
import { AddFreiberuflerComponent } from './freiberufler/add-freiberufler/add-freiberufler.component';
import { AddmanagerComponent } from './Manager/add-manager/add-manager.component';
import { AddangestellterComponent } from './angestellter/add-angestellter/add-angestellter.component';
import { AddDepartmentComponent } from './department/add-department/add-department.component';

@NgModule({
  declarations: [
    AppComponent,
    AngestellterComponent,
    FreiberuflerComponent,
    ManagerComponent,
    MitarbeiterComponent,
    EmployeesComponent,
    DepartmentComponent,
    AddMitarbeiterComponent,
    AddFreiberuflerComponent,
    AddmanagerComponent,
    AddangestellterComponent,
    AddDepartmentComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    DataTablesModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
