

export class Mitrabeiter {
    mitarbeiter_id:number;
    public firstname:String;
    public lastname:String;
    public middlename:String;
    public arbeitsBeginn:Date;
    public steuerNummer:String;
    public typus:String;
}
