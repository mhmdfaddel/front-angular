import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MitarbeiterService {

  private baseUrl = 'http://localhost:8080/';  
  
  constructor(private http:HttpClient) { } 

  getMitarbeiterList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'mitarbeiter');  
  }  
  
  createMitarbeiter(mitarbeiter: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'mitarbeiter', mitarbeiter);  
  }  
  
  deleteMitarbeiter(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/mitarbeiter/${id}`, { responseType: 'text' });  
  }  
  
  getMitarbeiter(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/mitarbeiter/${id}`);  
  }  
  
  updateMitarbeiter(id: number, value: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}/mitarbeiter/${id}`, value);  
  }

  getDepartmentList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'department');  
  }  
  
  createDepartment(department: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'department', department);  
  }  
  
  deleteDepartment(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/department/${id}`, { responseType: 'text' });  
  }  
  
  getDepartment(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/department/${id}`);  
  }  
  
  updateDepartment(id: number, value: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}/department/${id}`, value);  
  }
}
