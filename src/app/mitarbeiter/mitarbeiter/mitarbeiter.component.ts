import { Component, OnInit } from '@angular/core';
import { MitarbeiterService } from '../mitarbeiter.service';
import { Observable,Subject } from "rxjs";
import * as XLSX from 'xlsx';

import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Mitrabeiter } from '../mitarbeiter';
import { Department } from 'src/app/department/department';
import { DepartmentService } from 'src/app/department/department.service';

@Component({
  selector: 'app-mitarbeiter',
  templateUrl: './mitarbeiter.component.html',
  styleUrls: ['./mitarbeiter.component.css']
})
export class MitarbeiterComponent implements OnInit {

 constructor(private mitarbeiterservice:MitarbeiterService, private departmentservice:DepartmentService) { }

  mitarbeitersArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();


  mitarbeiters: Observable<Mitrabeiter[]>;
  mitarbeiter : Mitrabeiter=new Mitrabeiter();
  departments: Observable<Department[]>;
  department : Department=new Department();
  deleteMessage=false;
  mitarbeiterlist:any;
  departmentlist:any;

  isupdated = false;    
 

  ngOnInit() {
    this.isupdated=false;
    this.dtOptions = {
      pageLength: 6,
      stateSave:true,
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],
      processing: true
    };   
    this.mitarbeiterservice.getMitarbeiterList().subscribe(data =>{
    this.mitarbeiters =data;
    console.log(data);
    this.dtTrigger.next();
    })

    this.departmentservice.getDepartmentList().subscribe(data =>{
      this.departments =data;
      this.dtTrigger.next();
      })
  }
  
  deleteMitarbeiter(id: number) {
    this.mitarbeiterservice.deleteMitarbeiter(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.mitarbeiterservice.getMitarbeiterList().subscribe(data =>{
            this.mitarbeiters =data
            })
        },
        error => console.log(error));
  }


  updateMitarbeiter(id: number){
    this.mitarbeiterservice.getMitarbeiter(id)
      .subscribe(
        data => {
          this.mitarbeiterlist=data           
        },
        error => console.log(error));
  }

  mitarbeiterupdateform=new FormGroup({
    mitarbeiter_id:new FormControl(),
    mitarbeiter_firstname:new FormControl(),
    mitarbeiter_middlename:new FormControl(),
    mitarbeiter_lastname:new FormControl(),
    mitarbeiter_ArbeitsBeginn:new FormControl(),
    mitarbeiter_SteuerNummer:new FormControl(),
    mitarbeiter_typus:new FormControl()
  });

  updateStu(updstu){
    this.mitarbeiter=new Mitrabeiter(); 
   this.mitarbeiter.mitarbeiter_id=this.MitarbeiterId.value;
   this.mitarbeiter.firstname=this.mitarbeiterFirstName.value;
   this.mitarbeiter.middlename=this.mitarbeiterMiddleName.value;
   this.mitarbeiter.lastname=this.mitarbeiterLastName.value;
   this.mitarbeiter.arbeitsBeginn = this.mitarbeiterArbeitsBeginn.value;
   this.mitarbeiter.steuerNummer = this.mitarbeiterSteuerNummer.value;
   this.mitarbeiter.typus = this.mitarbeiterTypus.value;
   this.mitarbeiterservice.updateMitarbeiter(this.mitarbeiter.mitarbeiter_id,this.mitarbeiter).subscribe(
    data => {     
      this.isupdated=true;
      this.mitarbeiterservice.getMitarbeiterList().subscribe(data =>{
        this.mitarbeiters =data
        })
    },
    error => console.log(error));
  }

  get MitarbeiterId(){
    return this.mitarbeiterupdateform.get('mitarbeiter_id');
  }

  get mitarbeiterFirstName(){
    return this.mitarbeiterupdateform.get('mitarbeiter_firstname');
  }
  get mitarbeiterMiddleName(){
    return this.mitarbeiterupdateform.get('mitarbeiter_middlename');
  }
  get mitarbeiterLastName(){
    return this.mitarbeiterupdateform.get('mitarbeiter_lastname');
  }

  get mitarbeiterArbeitsBeginn(){
    return this.mitarbeiterupdateform.get('mitarbeiter_ArbeitsBeginn');
  }

  get mitarbeiterSteuerNummer(){
    return this.mitarbeiterupdateform.get('mitarbeiter_SteuerNummer');
  }

  get mitarbeiterTypus(){
    return this.mitarbeiterupdateform.get('mitarbeiter_typus');
  }

  changeisUpdate(){
    this.isupdated=false;
  }


  exportexcel(): void 
  {
     let element = document.getElementById('mitarbeiters'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     XLSX.writeFile(wb,'mitarbeiters.xlsx');
    
  }

  deleteDepartment(id: number) {
    this.departmentservice.deleteDepartment(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.departmentservice.getDepartmentList().subscribe(data =>{
            this.departments =data
            })
        },
        error => console.log(error));
  }


  updateDepartment(id: number){
    this.departmentservice.getDepartment(id)
      .subscribe(
        data => {
          this.departmentlist=data           
        },
        error => console.log(error));
  }

  departmentupdateform=new FormGroup({
    department_id:new FormControl(),
    department_dep_manager:new FormControl(),
    department_dep_name:new FormControl(),

  });

  updateDept(updDept){
    this.department=new Department(); 
   this.department.department_id=this.DepartmentId.value;
   this.department.dep_manager=this.departmentManager.value;
   this.department.dep_name=this.departmentName.value;
 this.departmentservice.updateDepartment(this.department.department_id,this.department).subscribe(
    data => {     
      this.isupdated=true;
      this.departmentservice.getDepartmentList().subscribe(data =>{
        this.departments =data
        })
    },
    error => console.log(error));
  }

  get DepartmentId(){
    return this.departmentupdateform.get('department_id');
  }

  get departmentManager(){
    return this.departmentupdateform.get('department_dep_manager');
  }
  get departmentName(){
    return this.departmentupdateform.get('department_dep_name');
  }


  changeisUpdatedept(){
    this.isupdated=false;
  }


  exportexceldept(): void 
  {
     let element = document.getElementById('departments'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     XLSX.writeFile(wb,'departments.xlsx');
    
  }
}
