import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Mitrabeiter } from '../mitarbeiter';
import { MitarbeiterService } from '../mitarbeiter.service';
@Component({
  selector: 'app-add-mitarbeiter',
  templateUrl: './add-mitarbeiter.component.html',
  styleUrls: ['./add-mitarbeiter.component.css']
})
export class AddMitarbeiterComponent implements OnInit {

  constructor(private mitarbeiterservice:MitarbeiterService) { }

  mitarbeiter : Mitrabeiter=new Mitrabeiter();
  submitted = false;

  ngOnInit() {
    this.submitted=false;
  }

  mitarbeitersaveform=new FormGroup({
    mitarbeiter_firstname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    mitarbeiter_middlename:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    mitarbeiter_lastname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    mitarbeiter_arbeitsBeginn:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    mitarbeiter_steuerNummer:new FormControl(''),
    mitarbeiter_branch:new FormControl()
  });

  saveMitarbeiter(saveMitarbeiter){
    this.mitarbeiter=new Mitrabeiter();   
    this.mitarbeiter.firstname=this.MitarbeiterFirstName.value;
    this.mitarbeiter.middlename=this.MitarbeiterMiddleName.value;
    this.mitarbeiter.lastname=this.MitarbeiterLastName.value;
    this.mitarbeiter.arbeitsBeginn=this.MitarbeiterarbeitsBeginn.value;

    this.mitarbeiter.steuerNummer=this.MitarbeiterSteuerNummer.value;
    this.mitarbeiter.typus=this.MitarbeiterBranch.value;
    this.submitted = true;
    this.save();
  }

  

  save() {
    this.mitarbeiterservice.createMitarbeiter(this.mitarbeiter)
      .subscribe(data => console.log(data), error => console.log(error));
      console.log(this.mitarbeiter);
    this.mitarbeiter = new Mitrabeiter();
  }

  get MitarbeiterFirstName(){
    return this.mitarbeitersaveform.get('mitarbeiter_firstname');
  }

  get MitarbeiterMiddleName(){
    return this.mitarbeitersaveform.get('mitarbeiter_middlename');
  }

  get MitarbeiterLastName(){
    return this.mitarbeitersaveform.get('mitarbeiter_lastname');
  }

  get MitarbeiterSteuerNummer(){
    return this.mitarbeitersaveform.get('mitarbeiter_steuerNummer');
  }

  get MitarbeiterarbeitsBeginn(){
    return this.mitarbeitersaveform.get('mitarbeiter_arbeitsBeginn');
  }

  get MitarbeiterBranch(){
    return this.mitarbeitersaveform.get('mitarbeiter_branch');
  }

  addMitarbeiterForm(){
    this.submitted=false;
    this.mitarbeitersaveform.reset();
  }
}
