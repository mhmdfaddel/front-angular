import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Angestellter } from '../angestellter';
import { AngestellterService } from '../angestellter.service';
@Component({
  selector: 'app-add-angestellter',
  templateUrl: './add-angestellter.component.html',
  styleUrls: ['./add-angestellter.component.css']
})
export class AddangestellterComponent implements OnInit {

  constructor(private angestellterservice:AngestellterService) { }

  angestellter : Angestellter=new Angestellter();
  submitted = false;

  ngOnInit() {
    this.submitted=false;
  }

  angestelltersaveform=new FormGroup({
    angestellter_firstname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    angestellter_middlename:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    angestellter_lastname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),

    angestellter_ArbeitsBeginn:new FormControl(''),
    angestellter_SteuerNummer:new FormControl(),
    angestellter_Gehalt:new FormControl
  });

  saveAngestellter(saveAngestellter){
    this.angestellter=new Angestellter();   
    this.angestellter.firstname=this.angestellterFirstName.value;
    this.angestellter.middlename=this.angestellterMiddleName.value;
    this.angestellter.lastname=this.angestellterLastName.value;
    this.angestellter.gehalt = this.angestellterGehalt.value;
    this.angestellter.arbeitsBeginn = this.angestellterArbeitsBeginn.value;
    this.angestellter.steuerNummer = this.angestellterSteuerNummer.value;
    this.submitted = true;
    this.save();
  }

  

  save() {
    this.angestellterservice.createAngestellter(this.angestellter)
      .subscribe(data => console.log(data), error => console.log(error));
    this.angestellter = new Angestellter();
  }

  get angestellterFirstName(){
    return this.angestelltersaveform.get('angestellter_firstname');
  }
  get angestellterMiddleName(){
    return this.angestelltersaveform.get('angestellter_middlename');
  }
  get angestellterLastName(){
    return this.angestelltersaveform.get('angestellter_lastname');
  }

  get angestellterArbeitsBeginn(){
    return this.angestelltersaveform.get('angestellter_ArbeitsBeginn');
  }

  get angestellterSteuerNummer(){
    return this.angestelltersaveform.get('angestellter_SteuerNummer');
  }
  get angestellterGehalt(){
    return this.angestelltersaveform.get('angestellter_Gehalt');
  }


  addangestellterForm(){
    this.submitted=false;
    this.angestelltersaveform.reset();
  }
}
