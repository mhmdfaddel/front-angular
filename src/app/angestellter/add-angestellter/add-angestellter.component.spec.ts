import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddAngestellterComponent } from './add-angestellter.component';

describe('AddAngestellterComponent', () => {
  let component: AddAngestellterComponent;
  let fixture: ComponentFixture<AddAngestellterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddAngestellterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddAngestellterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
