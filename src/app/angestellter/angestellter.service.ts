import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AngestellterService {

  private baseUrl = 'http://localhost:8080/';  
  
  constructor(private http:HttpClient) { } 

  getAngestellterList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'angestellter');  
  }  
  
  createAngestellter(angestellter: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'angestellter', angestellter);  
  }  
  
  deleteAngestellter(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/angestellter/${id}`, { responseType: 'text' });  
  }  
  
  getAngestellter(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/angestellter/${id}`);  
  }  
  
  updateAngestellter(id: number, value: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}/angestellter/${id}`, value);  
  }
}
