import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AngestellterComponent } from './angestellter.component';

describe('AngestellterComponent', () => {
  let component: AngestellterComponent;
  let fixture: ComponentFixture<AngestellterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AngestellterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AngestellterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
