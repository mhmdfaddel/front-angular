import { Component, OnInit } from '@angular/core';
import { AngestellterService } from '../angestellter.service';
import { Angestellter } from '../angestellter';
import { Observable,Subject } from "rxjs";
import * as XLSX from 'xlsx';

import {FormControl,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-angestellter',
  templateUrl: './angestellter.component.html',
  styleUrls: ['./angestellter.component.css']
})
export class AngestellterComponent implements OnInit {

 constructor(private angestellterservice:AngestellterService) { }

  angestelltersArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();


  angestellters: Observable<Angestellter[]>;
  angestellter : Angestellter=new Angestellter();
  deleteMessage=false;
  angestellterlist:any;
  isupdated = false;    
 

  ngOnInit() {
    this.isupdated=false;
    this.dtOptions = {
      pageLength: 6,
      stateSave:true,
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],
      processing: true
    };   
    this.angestellterservice.getAngestellterList().subscribe(data =>{
    this.angestellters =data;
    this.dtTrigger.next();
    })
  }
  
  deleteAngestellter(id: number) {
    this.angestellterservice.deleteAngestellter(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.angestellterservice.getAngestellterList().subscribe(data =>{
            this.angestellters =data
            })
        },
        error => console.log(error));
  }


  updateAngestellter(id: number){
    this.angestellterservice.getAngestellter(id)
      .subscribe(
        data => {
          this.angestellterlist=data           
        },
        error => console.log(error));
  }

  angestellterupdateform=new FormGroup({
    angestellter_id:new FormControl(),
    angestellter_firstname:new FormControl(),
    angestellter_middlename:new FormControl(),
    angestellter_lastname:new FormControl(),
    angestellter_ArbeitsBeginn:new FormControl(),
    angestellter_SteuerNummer:new FormControl(),
    angestellter_Gehalt:new FormControl(),
    angestellter_Tel:new FormControl(),
    angestellter_Auto:new FormControl(),

  });

  updateStu(updstu){
    this.angestellter=new Angestellter(); 
   this.angestellter.angestellter_id=this.AngestellterId.value;
   this.angestellter.firstname=this.angestellterFirstName.value;
   this.angestellter.middlename=this.angestellterMiddleName.value;
   this.angestellter.lastname=this.angestellterLastName.value;
   this.angestellter.arbeitsBeginn = this.angestellterArbeitsBeginn.value;
   this.angestellter.steuerNummer = this.angestellterSteuerNummer.value;
   this.angestellter.gehalt = this.angestellterGehalt.value;

   this.angestellterservice.updateAngestellter(this.angestellter.angestellter_id,this.angestellter).subscribe(
    data => {     
      this.isupdated=true;
      this.angestellterservice.getAngestellterList().subscribe(data =>{
        this.angestellters =data
        })
    },
    error => console.log(error));
  }

  get AngestellterId(){
    return this.angestellterupdateform.get('angestellter_id');
  }

  get angestellterFirstName(){
    return this.angestellterupdateform.get('angestellter_firstname');
  }
  get angestellterMiddleName(){
    return this.angestellterupdateform.get('angestellter_middlename');
  }
  get angestellterLastName(){
    return this.angestellterupdateform.get('angestellter_lastname');
  }

  get angestellterArbeitsBeginn(){
    return this.angestellterupdateform.get('angestellter_ArbeitsBeginn');
  }

  get angestellterSteuerNummer(){
    return this.angestellterupdateform.get('angestellter_SteuerNummer');
  }

  get angestellterGehalt(){
    return this.angestellterupdateform.get('angestellter_Gehalt');
  }
  changeisUpdate(){
    this.isupdated=false;
  }


  exportexcel(): void 
  {
     let element = document.getElementById('angestellters'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     XLSX.writeFile(wb,'angestellters.xlsx');
    
  }
}
