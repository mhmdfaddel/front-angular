import { TestBed } from '@angular/core/testing';

import { AngestellterService } from './angestellter.service';

describe('AngestellterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AngestellterService = TestBed.get(AngestellterService);
    expect(service).toBeTruthy();
  });
});
