import { Component, OnInit } from '@angular/core';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Manager } from '../manager';
import { ManagerService } from '../manager.service';
@Component({
  selector: 'app-add-manager',
  templateUrl: './add-manager.component.html',
  styleUrls: ['./add-manager.component.css']
})
export class AddmanagerComponent implements OnInit {

  constructor(private managerservice:ManagerService) { }

  manager : Manager=new Manager();
  submitted = false;

  ngOnInit() {
    this.submitted=false;
  }

  managersaveform=new FormGroup({
    manager_firstname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    manager_middlename:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    manager_lastname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),

    manager_ArbeitsBeginn:new FormControl(''),
    manager_SteuerNummer:new FormControl(),
    manager_Auto:new FormControl(),
    manager_Tel:new FormControl(),
    manager_Gehalt:new FormControl()
  });

   savemanager (savemanager){
     console.log("Hello");
    this.manager=new Manager();   
    this.manager.firstname=this.managerFirstName.value;
    this.manager.middlename=this.managerMiddleName.value;
    this.manager.lastname=this.managerLastName.value;
    this.manager.auto = this.managerAuto.value;
    this.manager.arbeitsBeginn = this.managerArbeitsBeginn.value;
    this.manager.steuerNummer = this.managerSteuerNummer.value;
    this.manager.tel = this.managerTel.value;
    this.manager.gehalt = this.managerGehalt.value;
    this.submitted = true;
    this.save();
  }

  

  save() {
    this.managerservice.createManager(this.manager)
      .subscribe(data => console.log(data), error => console.log(error));
      console.log(this.manager);
    this.manager = new Manager();
  }

  get managerFirstName(){
    return this.managersaveform.get('manager_firstname');
  }
  get managerMiddleName(){
    return this.managersaveform.get('manager_middlename');
  }
  get managerLastName(){
    return this.managersaveform.get('manager_lastname');
  }

  get managerArbeitsBeginn(){
    return this.managersaveform.get('manager_ArbeitsBeginn');
  }

  get managerSteuerNummer(){
    return this.managersaveform.get('manager_SteuerNummer');
  }
  get managerTel(){
    return this.managersaveform.get('manager_Tel');
  }
  get managerAuto(){
    return this.managersaveform.get('manager_Auto');
  }
  get managerGehalt(){
    return this.managersaveform.get('manager_Gehalt');
  }

  addmanagerForm(){
    this.submitted=false;
    this.managersaveform.reset();
  }
}
