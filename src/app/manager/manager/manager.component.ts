import { Component, OnInit } from '@angular/core';
import { ManagerService } from '../manager.service';
import { Manager } from '../manager';
import { Observable,Subject } from "rxjs";
import * as XLSX from 'xlsx';

import {FormControl,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagerComponent implements OnInit {

 constructor(private managerservice:ManagerService) { }

  managersArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();


  managers: Observable<Manager[]>;
  manager : Manager=new Manager();
  deleteMessage=false;
  managerlist:any;
  isupdated = false;    
 

  ngOnInit() {
    this.isupdated=false;
    this.dtOptions = {
      pageLength: 6,
      stateSave:true,
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],
      processing: true
    };   
    this.managerservice.getManagerList().subscribe(data =>{
    this.managers =data;
    this.dtTrigger.next();
    })
  }
  
  deleteManager(id: number) {
    this.managerservice.deleteManager(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.managerservice.getManagerList().subscribe(data =>{
            this.managers =data
            })
        },
        error => console.log(error));
  }


  updateManager(id: number){
    this.managerservice.getManager(id)
      .subscribe(
        data => {
          this.managerlist=data           
        },
        error => console.log(error));
  }

  managerupdateform=new FormGroup({
    manager_id:new FormControl(),
    manager_firstname:new FormControl(),
    manager_middlename:new FormControl(),
    manager_lastname:new FormControl(),
    manager_ArbeitsBeginn:new FormControl(),
    manager_SteuerNummer:new FormControl(),
    manager_Gehalt:new FormControl(),
    manager_Tel:new FormControl(),
    manager_Auto:new FormControl(),

  });

  updateStu(updstu){
    this.manager=new Manager(); 
   this.manager.manager_id=this.ManagerId.value;
   this.manager.firstname=this.managerFirstName.value;
   this.manager.middlename=this.managerMiddleName.value;
   this.manager.lastname=this.managerLastName.value;
   this.manager.auto = this.managerAuto.value;
   this.manager.arbeitsBeginn = this.managerArbeitsBeginn.value;
   this.manager.steuerNummer = this.managerSteuerNummer.value;
   this.manager.tel = this.managerTel.value;
   this.manager.gehalt = this.managerGehalt.value;

   this.managerservice.updateManager(this.manager.manager_id,this.manager).subscribe(
    data => {     
      this.isupdated=true;
      this.managerservice.getManagerList().subscribe(data =>{
        this.managers =data
        })
    },
    error => console.log(error));
  }

  get ManagerId(){
    return this.managerupdateform.get('manager_id');
  }

  get managerFirstName(){
    return this.managerupdateform.get('manager_firstname');
  }
  get managerMiddleName(){
    return this.managerupdateform.get('manager_middlename');
  }
  get managerLastName(){
    return this.managerupdateform.get('manager_lastname');
  }

  get managerArbeitsBeginn(){
    return this.managerupdateform.get('manager_ArbeitsBeginn');
  }

  get managerSteuerNummer(){
    return this.managerupdateform.get('manager_SteuerNummer');
  }
  get managerTel(){
    return this.managerupdateform.get('manager_Tel');
  }
  get managerAuto(){
    return this.managerupdateform.get('manager_Auto');
  }
  get managerGehalt(){
    return this.managerupdateform.get('manager_Gehalt');
  }
  changeisUpdate(){
    this.isupdated=false;
  }


  exportexcel(): void 
  {
     let element = document.getElementById('managers'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     XLSX.writeFile(wb,'managers.xlsx');
    
  }
}
