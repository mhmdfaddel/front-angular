import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerService {

  private baseUrl = 'http://localhost:8080/';  
  
  constructor(private http:HttpClient) { } 

  getManagerList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'manager');  
  }  
  
  createManager(manager: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'manager', manager);  
  }  
  
  deleteManager(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/manager/${id}`, { responseType: 'text' });  
  }  
  
  getManager(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/manager/${id}`);  
  }  
  
  updateManager(id: number, value: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}/manager/${id}`, value);  
  }
}
