import { Component, OnInit } from '@angular/core';
import { FreiberuflerService } from '../freiberufler.service';
import { Freiberufler } from '../freiberufler';
import { Observable,Subject } from "rxjs";
import * as XLSX from 'xlsx';

import {FormControl,FormGroup,Validators} from '@angular/forms';

@Component({
  selector: 'app-freiberufler',
  templateUrl: './freiberufler.component.html',
  styleUrls: ['./freiberufler.component.css']
})
export class FreiberuflerComponent implements OnInit {

 constructor(private freiberuflerservice:FreiberuflerService) { }

  freiberuflersArray: any[] = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any>= new Subject();


  freiberuflers: Observable<Freiberufler[]>;
  freiberufler : Freiberufler=new Freiberufler();
  deleteMessage=false;
  freiberuflerlist:any;
  isupdated = false;    
 

  ngOnInit() {
    this.isupdated=false;
    this.dtOptions = {
      pageLength: 6,
      stateSave:true,
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],
      processing: true
    };   
    this.freiberuflerservice.getFreiberuflerList().subscribe(data =>{
    this.freiberuflers =data;
    this.dtTrigger.next();
    })
  }
  
  deleteFreiberufler(id: number) {
    this.freiberuflerservice.deleteFreiberufler(id)
      .subscribe(
        data => {
          console.log(data);
          this.deleteMessage=true;
          this.freiberuflerservice.getFreiberuflerList().subscribe(data =>{
            this.freiberuflers =data
            })
        },
        error => console.log(error));
  }


  updateFreiberufler(id: number){
    this.freiberuflerservice.getFreiberufler(id)
      .subscribe(
        data => {
          this.freiberuflerlist=data           
        },
        error => console.log(error));
  }

  freiberuflerupdateform=new FormGroup({
    freiberufler_id:new FormControl(),
    freiberufler_firstname:new FormControl(),
    freiberufler_middlename:new FormControl(),
    freiberufler_lastname:new FormControl(),
    freiberufler_ArbeitsBeginn:new FormControl(),
    freiberufler_SteuerNummer:new FormControl(),
    freiberufler_Gehalt:new FormControl(),
    freiberufler_Tel:new FormControl(),
    freiberufler_Auto:new FormControl(),
    freiberufler_StdSatz:new FormControl(),
    freiberufler_Stunde:new FormControl(),
  });

  updateStu(updstu){
    this.freiberufler=new Freiberufler(); 
   this.freiberufler.freiberufler_id=this.FreiberuflerId.value;
   this.freiberufler.firstname=this.freiberuflerFirstName.value;
   this.freiberufler.middlename=this.freiberuflerMiddleName.value;
   this.freiberufler.lastname=this.freiberuflerLastName.value;
   this.freiberufler.arbeitsBeginn = this.freiberuflerArbeitsBeginn.value;
   this.freiberufler.steuerNummer = this.freiberuflerSteuerNummer.value;
  this.freiberufler.stdSatz = this.freiberuflerStdSatz.value;
  this.freiberufler.Stunde = this.freiberuflerStunde.value;
   this.freiberuflerservice.updateFreiberufler(this.freiberufler.freiberufler_id,this.freiberufler).subscribe(
    data => {     
      this.isupdated=true;
      this.freiberuflerservice.getFreiberuflerList().subscribe(data =>{
        this.freiberuflers =data
        })
    },
    error => console.log(error));
  }

  get FreiberuflerId(){
    return this.freiberuflerupdateform.get('freiberufler_id');
  }

  get freiberuflerFirstName(){
    return this.freiberuflerupdateform.get('freiberufler_firstname');
  }
  get freiberuflerMiddleName(){
    return this.freiberuflerupdateform.get('freiberufler_middlename');
  }
  get freiberuflerLastName(){
    return this.freiberuflerupdateform.get('freiberufler_lastname');
  }

  get freiberuflerArbeitsBeginn(){
    return this.freiberuflerupdateform.get('freiberufler_ArbeitsBeginn');
  }

  get freiberuflerSteuerNummer(){
    return this.freiberuflerupdateform.get('freiberufler_SteuerNummer');
  }

  get freiberuflerStdSatz(){
    return this.freiberuflerupdateform.get('freiberufler_StdSatz');
  }
  get freiberuflerStunde(){
    return this.freiberuflerupdateform.get('freiberufler_Stunde');
  }
  changeisUpdate(){
    this.isupdated=false;
  }


  exportexcel(): void 
  {
     let element = document.getElementById('freiberuflers'); 
     const ws: XLSX.WorkSheet =XLSX.utils.table_to_sheet(element);

     const wb: XLSX.WorkBook = XLSX.utils.book_new();
     XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

     XLSX.writeFile(wb,'freiberuflers.xlsx');
    
  }
}
