import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreiberuflerComponent } from './freiberufler.component';

describe('FreiberuflerComponent', () => {
  let component: FreiberuflerComponent;
  let fixture: ComponentFixture<FreiberuflerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreiberuflerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreiberuflerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
