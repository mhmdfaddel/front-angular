import { TestBed } from '@angular/core/testing';

import { FreiberuflerService } from './freiberufler.service';

describe('FreiberuflerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FreiberuflerService = TestBed.get(FreiberuflerService);
    expect(service).toBeTruthy();
  });
});
