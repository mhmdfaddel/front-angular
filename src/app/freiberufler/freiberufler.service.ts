import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FreiberuflerService {

  private baseUrl = 'http://localhost:8080/';  
  
  constructor(private http:HttpClient) { } 

  getFreiberuflerList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'freiberufler');  
  }  
  
  createFreiberufler(freiberufler: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'freiberufler', freiberufler);  
  }  
  
  deleteFreiberufler(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/freiberufler/${id}`, { responseType: 'text' });  
  }  
  
  getFreiberufler(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/freiberufler/${id}`);  
  }  
  
  updateFreiberufler(id: number, value: any): Observable<Object> {  
    return this.http.put(`${this.baseUrl}/freiberufler/${id}`, value);  
  }
}
