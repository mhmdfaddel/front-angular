import { Component, OnInit } from '@angular/core';
import { FreiberuflerService } from '../freiberufler.service';
import {FormControl,FormGroup,Validators} from '@angular/forms';
import { Freiberufler } from '../freiberufler';
@Component({
  selector: 'app-add-freiberufler',
  templateUrl: './add-freiberufler.component.html',
  styleUrls: ['./add-freiberufler.component.css']
})
export class AddFreiberuflerComponent implements OnInit {

  constructor(private freiberuflerservice:FreiberuflerService) { }

  freiberufler : Freiberufler=new Freiberufler();
  submitted = false;

  ngOnInit() {
    this.submitted=false;
  }

  freiberuflersaveform=new FormGroup({
    freiberufler_firstname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    freiberufler_middlename:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),
    freiberufler_lastname:new FormControl('' , [Validators.required , Validators.minLength(5) ] ),

    freiberufler_ArbeitsBeginn:new FormControl(''),
    freiberufler_SteuerNummer:new FormControl(),
    freiberufler_StdSatz:new FormControl(),
    freiberufler_Stunde:new FormControl
  });

  saveFreiberufler(saveFreiberufler){
    this.freiberufler=new Freiberufler();   
    this.freiberufler.firstname=this.FreiberuflerFirstName.value;
    this.freiberufler.middlename=this.FreiberuflerMiddleName.value;
    this.freiberufler.lastname=this.FreiberuflerLastName.value;
    this.freiberufler.Stunde = this.FreiberuflerStunde.value;
    this.freiberufler.arbeitsBeginn = this.FreiberuflerArbeitsBeginn.value;
    this.freiberufler.steuerNummer = this.FreiberuflerSteuerNummer.value;
    this.freiberufler.stdSatz = this.FreiberuflerStdSatz.value;
    this.submitted = true;
    this.save();
  }

  

  save() {
    this.freiberuflerservice.createFreiberufler(this.freiberufler)
      .subscribe(data => console.log(data), error => console.log(error));
    this.freiberufler = new Freiberufler();
  }

  get FreiberuflerFirstName(){
    return this.freiberuflersaveform.get('freiberufler_firstname');
  }
  get FreiberuflerMiddleName(){
    return this.freiberuflersaveform.get('freiberufler_middlename');
  }
  get FreiberuflerLastName(){
    return this.freiberuflersaveform.get('freiberufler_lastname');
  }

  get FreiberuflerArbeitsBeginn(){
    return this.freiberuflersaveform.get('freiberufler_ArbeitsBeginn');
  }

  get FreiberuflerSteuerNummer(){
    return this.freiberuflersaveform.get('freiberufler_SteuerNummer');
  }
  get FreiberuflerStdSatz(){
    return this.freiberuflersaveform.get('freiberufler_StdSatz');
  }
  get FreiberuflerStunde(){
    return this.freiberuflersaveform.get('freiberufler_Stunde');
  }

  addFreiberuflerForm(){
    this.submitted=false;
    this.freiberuflersaveform.reset();
  }
}
