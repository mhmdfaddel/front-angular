import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddFreiberuflerComponent } from './add-freiberufler.component';

describe('AddFreiberuflerComponent', () => {
  let component: AddFreiberuflerComponent;
  let fixture: ComponentFixture<AddFreiberuflerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFreiberuflerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddFreiberuflerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
